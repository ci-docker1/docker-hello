app: a.o main.o
	g++ -o app a.o main.o

main.o: main.cpp a.h
	g++ -c main.cpp

a.o: a.cpp a.h
	g++ -c a.cpp